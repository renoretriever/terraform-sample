terraform {
  backend "s3" {
    bucket = "backetname"
    key    = "terraform.tfstate"
    region = "ap-northeast-1"
  }
}
