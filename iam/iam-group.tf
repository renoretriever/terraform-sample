module "iam_group_with_policies" {
  source = "terraform-aws-modules/iam/aws//modules/iam-group-with-policies"

  version = "~> 2.0"

  name = local.group_name

  group_users = local.group_users
  custom_group_policy_arns = [
    "arn:aws:iam::aws:policy/PowerUserAccess",
  ]
}
