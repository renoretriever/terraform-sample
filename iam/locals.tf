locals {
  pgp_key = "keybase:test"
  group_name = "test-groups"
  group_users = [
    "test01",
    "test02",
  ]
}
