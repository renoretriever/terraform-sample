module "iam-user-test01" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 2.0"

  name          = "test01"
  force_destroy = true

  pgp_key = local.pgp_key

  password_reset_required = true
}


module "iam-user-test02" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 2.0"

  name          = "test02"
  force_destroy = true

  pgp_key = local.pgp_key

  password_reset_required = true
}

