# Terraform Sample


## Credentials

- vim .aws-credensials(for fish)

    ```
    set -x AWS_ACCESS_KEY_ID XXXXX
    set -x AWS_SECRET_ACCESS_KEY YYYYY
    set -x AWS_DEFAULT_REGION ap-northeast-1
    set -x AWS_REGION ap-northeast-1
    ```

## Execute

- Read file

    ```
    $ source .aws-credensials
    ```

- execute

    ```
    $ terraform init
    $ terraform plan
    $ terraform apply
    ```


